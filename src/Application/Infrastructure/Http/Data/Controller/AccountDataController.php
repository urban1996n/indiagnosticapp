<?php

namespace App\Application\Infrastructure\Http\Data\Controller;

use GuzzleHttp\Exception\RequestException;
use App\Application\Infrastructure\Http\Data\Utils\EcomController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
/**
 * Class AccountDataController
 * @package App\Infrastructure\Http\Data\Controller
 */
final class AccountDataController extends EcomController
{
    
     /**
     * @param string
     */
    
    protected $apiKey;
    /**
     *@param SessionInterface $session     
     */
    protected $session;

    /** 
     * Login route 
     * @Route("/login", name="data_login")
     * @param Request $request
     */
    public function login(Request $request)
    {
        $name = $request->get('username');
        $password = $request->get('password');
        $client = new \GuzzleHttp\Client(); 
        $content = ["username"=>"{$name}","password"=>"{$password}"];
        try {        
            $response = $client->request('POST',
            EcomController::API_BASE_URL.'login',
            ["form_params"=>$content]) ;
        } catch (RequestException $e) {
            return $this->redirectToRoute('login_view',["error"=>true]);
        }
        $response = json_decode($response->getBody());
        $role = $response->role;
        $apiKey = $response->api_key;
        $username = $response->username;
        
        $this->session->set('apiKey', $apiKey);
        $this->session->set('role', $role);
        $this->session->set('username',$username);
        
        return $this->redirectToRoute('current_measure');
    }
    /**
     * @Route("/logout", name="data_logout") 
     */
    public function logout()
    {
        $this->session->invalidate();
        return $this->redirectToRoute('login_view');
    }

    /**
     * Register account 
     * @Route("/account/register", name="register_account")
     * @param Request $request
     */
    public function register(Request $request)
    {
        $username = $request->get('username');
        $surname = $request->get('surname');
        $name = $request->get('name');
        $password = $request->get('password');
        $role = $request->get('role');
        if($role == 1)
        {
            $role="ROLE_ADMIN";
        }else
        {
            $role="ROLE_USER";
        }
        $client = new \GuzzleHttp\Client(); 
        $content = json_encode(["username"=>"{$username}","password"=>"{$password}","name"=>"{$name}","surname"=>"{$surname}","role"=>"{$role}"]);
       
        try {
            $response = $client->request('POST',
            EcomController::API_BASE_URL.'account/new',
            ["body"=>$content]);
        } catch (ClientException $e) {
            echo Psr7\str($e->getRequest());
            echo Psr7\str($e->getResponse());
        }
        return new Response($response->getBody());
    }

    /**
     * Change credentials for account 
     * @Route("/account/changeCredentials", name="account_change_credentials")
     * @param Request $request
     */
    public function cahngeCredentials(Request $request)
    {
        $username = $request->get('username');
        $name = $request->get('name');
        $surname = $request->get('surname');
        $password = $request->get('password');
        $id = $request->get('id');
        $role = $request->get('role');
        if($role == 1)
        {
            $role="ROLE_ADMIN";
        }else
        {
            $role="ROLE_USER";
        }
        $client = new \GuzzleHttp\Client(); 
        $content = json_encode([
            'name'=> "{$name}",
            'username'=> "{$username}",
            'surname'=> "{$surname}",
            'apiKey' => "{$this->apiKey}",
            'password'=>"{$password}",
            "role"=>"{$role}"
        ]);
        $response = $client->request('PUT',
        EcomController::API_BASE_URL.'users/'.$id,
        ["body"=>$content,'verify'=>false]);
        
        return new Response($response->getBody());
    }

    /**
     * Change credentials for account 
     * @Route("/account/getAll", name="get_all_accounts")
     */
    public function getAll()
    {
        $client = new \GuzzleHttp\Client(); 
        $content = json_encode([
            'apiKey' => "{$this->apiKey}"
        ]);
        $response = $client->request('GET',
        EcomController::API_BASE_URL.'users',
        ["body"=>$content]);
        
        return new Response($response->getBody());
    }

    /**
     * Deletes an account 
     * @Route("/account/delete", name="account_delete")
     * @param Request $request
     */
    public function deleteAccount(Request $request)
    {
        $id = $request->get('id');

        $client = new \GuzzleHttp\Client(); 
        $content=json_encode([
            'apiKey' => "{$this->apiKey}",
        ]);
        
        $response = $client->request('DELETE',
        EcomController::API_BASE_URL.'users/'.$id,
        ["body"=>$content]);
        
        return new Response($response->getBody());
    }
}
