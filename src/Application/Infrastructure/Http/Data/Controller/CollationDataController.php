<?php

namespace App\Application\Infrastructure\Http\Data\Controller;

use App\Application\Infrastructure\Http\Data\Utils\EcomController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
/**
 * Class ProductController
 * @package App\Infrastructure\Http\Data\Controller
 */
final class CollationDataController  extends EcomController
{

     /**
     * @param string
     */
    
    protected $apiKey;
    /**
     *@param SessionInterface $session     
     */
    protected $session;

    /**
     * Get currents from last machine's startPoint value by Id
     * @Route("/collation/current/{machineId}", name="current_data")
     * @param Request $request
     * @param int $machineId
     */
    public function currentDataCollaion(int $machineId){
        $client = new \GuzzleHttp\Client(); 
        $content = '{
            "apiKey": "'.$this->apiKey.'"
        }';
        $response = $client->request('GET',
        EcomController::API_BASE_URL."currents/id/{$machineId}/1",
        ["body"=>$content]);
        
        return new Response($response->getBody());
    }
    
    /**
     * Get currents from last machine's startPoint value by Id
     * @Route("/collation/range/{machineId}/{dateStart}/{dateStop}", name="data_machine_by_date_range")
     * @param int $machineId
     * @param string $dateStart
     * @param string $dateStop
     */
    public function machineDataByDateRange(int $machineId, string $dateStart, string $dateStop){
        $client = new \GuzzleHttp\Client(); 
        $content = '{
            "apiKey": "'.$this->apiKey.'"
        }';
        
        $dateStart = date("Y-m-j H:i:s",strtotime($dateStart));
        $dateStop = date("Y-m-j H:i:s",strtotime($dateStop));

        $response = $client->request('GET',
        EcomController::API_BASE_URL."currents/id/{$machineId}/date/$dateStart/$dateStop",
        ["body"=>$content]);
        
        return new Response($response->getBody());
    }

    /**
     * Get currents from last machine's startPoint value by Id
     * @Route("/collation/all/{dateStart}/{dateStop}", name="data_all_machine_by_date_range")
     * @param string $dateStart
     * @param string $dateStop
     */
    public function machineAllDataByDateRange(string $dateStart, string $dateStop){
        $client = new \GuzzleHttp\Client(); 
        $content = '{
            "apiKey": "'.$this->apiKey.'"
        }';
        
         $dateStart = date("Y-m-j H:i:s",strtotime($dateStart));
         $dateStop = date("Y-m-j H:i:s",strtotime($dateStop));

        $response = $client->request('GET',
        EcomController::API_BASE_URL."currents/all/$dateStart/$dateStop",
        ["body"=>$content]);
        
        return new Response($response->getBody());
    }

    /** Post Import to API
    * @Route("/collation/store", name="store_collation_data")    
    */
   public function storeCollation(){
       $client = new \GuzzleHttp\Client(); 
       $content = '{
           "apiKey": "'.$this->apiKey.'"
       }';
       $response = $client->request('GET',
       EcomController::API_BASE_URL.'public/index.php/api/collation/store',
       ["body"=>$content]);
       
       return new Response($response->getBody());
   }
}
