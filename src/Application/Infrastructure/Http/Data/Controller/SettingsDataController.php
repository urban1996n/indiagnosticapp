<?php

namespace App\Application\Infrastructure\Http\Data\Controller;

use App\Application\Infrastructure\Http\Data\Utils\EcomController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SettingsDataController
 * @package App\Infrastructure\Http\Data\Controller
 */
final class SettingsDataController extends EcomController
{ 
    /**
    * @param string
    */
   
   protected $apiKey;
   /**
    *@param SessionInterface $session     
    */
   protected $session;

    /**
     * Adds Settings entity 
     * @Route("/settings/add", name="settings_add")
     * @param Request $request
     */
    public function addSettings(Request $request)
    {
        $client = new \GuzzleHttp\Client(); 
        $machine_name = $request->get('machine_name');
        $client_name = $request->get('client_name');
        $ip_address = $request->get('ip_address');
        $ping_test = $request->get('ping_test');
        $http_test = $request->get('http_test');
        $other_test = $request->get('other_test');
        
        $content =json_encode([
            "apiKey"=>"{$this->apiKey}",
            "machine_name"=>$machine_name,
            "client_name"=>$client_name,
            "ip_address"=>$ip_address,
            "ping_test"=>$ping_test,
            "http_test"=>$http_test,
            "other_test"=>$other_test
            ]);
    
        $response = $client->request('POST',
        EcomController::API_BASE_URL.'settings',
        ["body"=>$content]);

        return new Response($response->getBody());
    }

    /**
     * Gets Settings entity by Id 
     * @Route("/settings/get/id/{$id}", name="settings_get_by_id")
     * @param int $id
     */
    public function getSettings(int $id): Response
    {
        $client = new \GuzzleHttp\Client(); 
        
        $content = json_encode([
            'apiKey' => "{$this->apiKey}"
        ]);
        
        $response = $client->request('GET',
        EcomController::API_BASE_URL."settings/id/{$id}",
        ["body"=>$content]);
        
        return new Response($response->getBody());
    }

    /**
     * Gets All Settings' entities
     * @Route("/settings/get/all", name="settings_get_all")
     */
    public function getAllSettingss(): Response
    {
        $client = new \GuzzleHttp\Client(); 
        
        $content = json_encode([
            'apiKey' => "{$this->apiKey}"
        ]);
        
        $response = $client->request('GET',
        EcomController::API_BASE_URL."settings",
        ["body"=>$content]);
        
        return new Response($response->getBody());
    }

    /**
     * Edits Settings entity 
     * @Route("/settings/edit", name="settings_edit")
     * @param Request $request
     */
    public function editSettings(Request $request): Response
    {
        $machine_name = $request->get('machine_name');
        $client_name = $request->get('client_name');
        $ip_address = $request->get('ip_address');
        $ping_test = $request->get('ping_test');
        $http_test = $request->get('http_test');
        $other_test = $request->get('other_test');

        $old_id = $request->get('old_id');

        $client = new \GuzzleHttp\Client(); 
        
        $content =json_encode([
            "apiKey"=>"{$this->apiKey}",
            "machine_name"=>$machine_name,
            "client_name"=>$client_name,
            "ip_address"=>$ip_address,
            "ping_test"=>$ping_test,
            "http_test"=>$http_test,
            "other_test"=>$other_test
        ]);
        
        $response = $client->request('PUT',
        EcomController::API_BASE_URL."settings/{$old_id}",
        ["body"=>$content]);
        return new Response($response->getBody());
    }

    /**
     * Edits Settings entity 
     * @Route("/settings/setSequence", name="set_sequence")
     * @param Request $request
     */
    public function setSequence(Request $request): Response
    {
        $machineId = $request->get('machineId');
        $startDate = $request->get('startDate');
        $endDate = $request->get('endDate');

        $client = new \GuzzleHttp\Client(); 
        $content =json_encode([
            "apiKey"=>"{$this->apiKey}",
            "endDate"=>$endDate,
            "startDate"=>$startDate
        ]);
        
        $response = $client->request('PUT',
        EcomController::API_BASE_URL."settings/sequence/{$machineId}",
        ["body"=>$content]);
        
        return new Response($content);
    }

    /**
     * Deletes Settings entity 
     * @Route("/settings/delete", name="settings_delete")
     * @param Request $request
     */
    public function deleteSettings(Request $request): Response
    {
        $id = $request->get('id');
            
        $client = new \GuzzleHttp\Client(); 
        
        $content = json_encode([
            'apiKey' => "{$this->apiKey}"
        ]);
        
        $response = $client->request('DELETE',
        EcomController::API_BASE_URL."settings/{$id}",
        ["body"=>$content]);
        
        return new Response($response->getBody());
    }
}
